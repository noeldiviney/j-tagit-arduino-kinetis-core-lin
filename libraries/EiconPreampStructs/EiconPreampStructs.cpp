
#include "EiconPreampStructs.h"
#include "Arduino.h"




// consider ASM for each of these, no rush;
float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
	if(out_max>out_min)
	{
		return (x-in_min)*(out_max-out_min)/(in_max-in_min)+out_min;
	} else {
		return -((x-in_max)*(out_max-out_min))/(in_min-in_max)+out_max;
	}
  return 0; // don't warn me about bullshit but by all means blow a few bytes THAT WILL NEVER EXECUTE DOPEY FUCKS>
}

float dBmap(float x, float out_min, float out_max, float virtcentre0)
{
	float nn=fmap(0.5,0,1,out_min,out_max);
	if(x<virtcentre0)
	{
		return fmap(x,0,virtcentre0,out_min,nn);
	} else {
		return fmap(x,virtcentre0,1,nn,out_max);
	}
}

filter_control_t mfilter(float portion, filter_control_t flt1, filter_control_t flt2)
{
  filter_control_t temp;
  temp.f=fmap(portion,0,1,flt1.f,flt2.f);
  temp.q=fmap(portion,0,1,flt1.q,flt2.q);
  temp.d=fmap(portion,0,1,flt1.d,flt2.d);
  return temp;
}

void calcBiquadf(uint8_t type, float Fc, float Q, float peakGain, float * coefs, float Fs)
{
	float n[5]={1,0,0,0,0};

	double norm;

	double V = pow(10, fabs(peakGain) / 20);
	double K = tan(M_PI * Fc / Fs);
	switch (type) {
/*    case "one-pole lp":
      coefs[3] = Math.exp(-2.0 * Math.PI * (Fc / Fs));
            coefs[0] = 1.0 - coefs[3];
            coefs[3] = -coefs[3];
      coefs[1] = coefs[2] = coefs[4] = 0;
      break;
            
    case "one-pole hp":
      coefs[3] = -Math.exp(-2.0 * Math.PI * (0.5 - Fc / Fs));
            coefs[0] = 1.0 + coefs[3];
            coefs[3] = -coefs[3];
      coefs[1] = coefs[2] = coefs[4] = 0;
      break;
                                  */
    case fltLoPass:
      norm = 1 / (1 + K / Q + K * K);
      n[0] = K * K * norm;
      n[1] = 2 * n[0];
      n[2] = n[0];
      n[3] = 2 * (K * K - 1) * norm;
      n[4] = (1 - K / Q + K * K) * norm;
    break;
    
    case fltHiPass:
      norm = 1 / (1 + K / Q + K * K);
      n[0] = 1 * norm;
      n[1] = -2 * n[0];
      n[2] = n[0];
      n[3] = 2 * (K * K - 1) * norm;
      n[4] = (1 - K / Q + K * K) * norm;
    break;
    
    case fltBandPass:
      norm = 1 / (1 + K / Q + K * K);
      n[0] = K / Q * norm;
      n[1] = 0;
      n[2] = -n[0];
      n[3] = 2 * (K * K - 1) * norm;
      n[4] = (1 - K / Q + K * K) * norm;
    break;
    
    case fltNotch:
      norm = 1 / (1 + K / Q + K * K);
      n[0] = (1 + K * K) * norm;
      n[1] = 2 * (K * K - 1) * norm;
      n[2] = n[0];
      n[3] = n[1];
      n[4] = (1 - K / Q + K * K) * norm;
    break;
    
    case fltParaEQ:
      if (peakGain >= 0) {
        norm = 1 / (1 + 1/Q * K + K * K);
        n[0] = (1 + V/Q * K + K * K) * norm;
        n[1] = 2 * (K * K - 1) * norm;
        n[2] = (1 - V/Q * K + K * K) * norm;
        n[3] = n[1];
        n[4] = (1 - 1/Q * K + K * K) * norm;
    } else {  
        norm = 1 / (1 + V/Q * K + K * K);
        n[0] = (1 + 1/Q * K + K * K) * norm;
        n[1] = 2 * (K * K - 1) * norm;
        n[2] = (1 - 1/Q * K + K * K) * norm;
        n[3] = n[1];
        n[4] = (1 - V/Q * K + K * K) * norm;
    }
    break;
    case fltLoShelf:
		if (peakGain >= 0) {
			norm = 1 / (1 + M_SQRT2 * K + K * K);
			n[0] = (1 + sqrt(2*V) * K + V * K * K) * norm;
			n[1] = 2 * (V * K * K - 1) * norm;
			n[2] = (1 - sqrt(2*V) * K + V * K * K) * norm;
			n[3] = 2 * (K * K - 1) * norm;
			n[4] = (1 - M_SQRT2 * K + K * K) * norm;
		} else {  
			norm = 1 / (1 + sqrt(2*V) * K + V * K * K);
			n[0] = (1 + M_SQRT2 * K + K * K) * norm;
			n[1] = 2 * (K * K - 1) * norm;
			n[2] = (1 - M_SQRT2 * K + K * K) * norm;
			n[3] = 2 * (V * K * K - 1) * norm;
			n[4] = (1 - sqrt(2*V) * K + V * K * K) * norm;
		}
    break;
    case fltHiShelf:
        if (peakGain >= 0) {
			norm = 1 / (1 + M_SQRT2 * K + K * K);
			n[0] = (V + sqrt(2*V) * K + K * K) * norm;
			n[1] = 2 * (K * K - V) * norm;
			n[2] = (V - sqrt(2*V) * K + K * K) * norm;
			n[3] = 2 * (K * K - 1) * norm;
			n[4] = (1 - M_SQRT2 * K + K * K) * norm;
        } else {  
			norm = 1 / (V + sqrt(2*V) * K + K * K);
			n[0] = (1 + M_SQRT2 * K + K * K) * norm;
			n[1] = 2 * (K * K - 1) * norm;
			n[2] = (1 - M_SQRT2 * K + K * K) * norm;
			n[3] = 2 * (K * K - V) * norm;
			n[4] = (V - sqrt(2*V) * K + K * K) * norm;
		}
	}

	(NVIC_DISABLE_IRQ(IRQ_SOFTWARE)); // __disable_irq(); // nasty could happen if we get half way through changing these
	coefs[0]=n[0];
	coefs[1]=n[1];
	coefs[2]=n[2]; // (because the way I use them, they could (*should* actually) be writing a live filter)
	coefs[3]=-n[3];
	coefs[4]=-n[4];
	(NVIC_ENABLE_IRQ(IRQ_SOFTWARE)); // __enable_irq(); // and the audio interrupt fires.
	
	
}

