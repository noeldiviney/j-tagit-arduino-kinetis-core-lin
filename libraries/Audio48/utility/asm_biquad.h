
#ifndef asm_biquad_h_
#define asm_biquad_h_

#ifdef __cplusplus
extern "C" {
#endif

void gasmBiquadifi(int16_t *audio_data16, float *inter_buffer, float *coefs);

void gasmInt2Float(int16_t* ptr16, float* ptrf);
void gasmInt2FloatPeak(int16_t* ptr16, float* ptrf, float* peaks);
void gasmFloat2Int(float* ptrf, int16_t* ptr16);
void gasmFloat2IntGain(float* ptrf, int16_t* ptr16, float* gain);
void gasmFloat2IntGainPeak(float* ptrf, int16_t* ptr16, float* gain, float* peak);
float gasmAbsf(float num);
void gasmBiquadf(float* ptrf, float* coefs);
void gasmBiquadff(float* dest, float* coefs, float* src);
void gasmMix2(float* buff0, float* buff1, float* gain);

void gasmCaterPeaks(float* peakinfo);
#ifdef __cplusplus
}
#endif


#endif
